LOCAL_PATH := $(call my-dir)

ifeq ($(call is-board-platform,msm8960),true)
include $(CLEAR_VARS)

LOCAL_MODULE :=	rf4ce
 
LOCAL_SRC_FILES := Projects/tools/LinuxHost/application/SimpleConsole_lnx.c
LOCAL_SRC_FILES += Projects/tools/LinuxHost/rtilib/rtis_lnx.c
LOCAL_SRC_FILES += Projects/tools/LinuxHost/ipclib/npi_lnx_i2c.c
LOCAL_SRC_FILES += Projects/tools/LinuxHost/ipclib/hal_i2c.c
LOCAL_SRC_FILES += Projects/tools/LinuxHost/ipclib/hal_gpio.c
LOCAL_SRC_FILES += Projects/tools/LinuxHost/ipclib/npi_lnx_spi.c
LOCAL_SRC_FILES += Projects/tools/LinuxHost/ipclib/npi_lnx_uart.c
LOCAL_SRC_FILES += Projects/tools/LinuxHost/ipclib/hal_spi.c
LOCAL_C_INCLUDES += $(LOCAL_PATH)/Projects/tools/LinuxHost/ipclib/
LOCAL_C_INCLUDES += $(LOCAL_PATH)/Projects/tools/LinuxHost/rtilib/
LOCAL_C_INCLUDES += $(LOCAL_PATH)/Projects/tools/LinuxHost/services/

LOCAL_CFLAGS := -DHAL_SPI=FALSE -DHAL_UART=FALSE -DHAL_I2C=TRUE -DMPQ8064_CDP -DRNP_HOST
LOCAL_C_INCLUDES += $(TARGET_OUT_INTERMEDIATES)/KERNEL_OBJ/usr/include/
LOCAL_ADDITIONAL_DEPENDENCIES := $(TARGET_OUT_INTERMEDIATES)/KERNEL_OBJ/usr
LOCAL_LDLIBS += -lpthread
LOCAL_SHARED_LIBRARIES := \
	libc

LOCAL_MODULE_TAGS := optional eng
include $(BUILD_EXECUTABLE)

include $(CLEAR_VARS)
LOCAL_MODULE       := RemoTI_RNP.cfg
LOCAL_MODULE_TAGS  := optional eng
LOCAL_MODULE_CLASS := ETC
LOCAL_SRC_FILES    := Projects/tools/LinuxHost/RemoTI_RNP.cfg
LOCAL_MODULE_PATH  := $(TARGET_OUT_ETC)
include $(BUILD_PREBUILT)

endif
