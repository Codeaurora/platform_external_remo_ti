RemoTI-Linux
============

Texas Instruments RF4CE application for a Linux host using a CC253x RNP

RemoTI is the implementation of the RF4Ce specification by Texas Instruments:
http://www.ti.com/remoti

more information can be found about this application on TI wiki site:
http://processors.wiki.ti.com/index.php/RF4CE,_linux_target_application#


2012, July 10th.
First Release.
